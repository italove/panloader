/*
 * © Copyright 2017-2018 The Panfrost Community
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#ifndef __MMAP_TRACE_H__
#define __MMAP_TRACE_H__

#include <mali-ioctl.h>
#include <list.h>
#include <stdlib.h>
#include <stddef.h>
#include "panwrap.h"

struct panwrap_allocated_memory {
	mali_ptr gpu_va;
	int flags;
	int allocation_number;
	size_t length;

	struct list node;
};

struct panwrap_mapped_memory {
	size_t length;

	void *addr;
	mali_ptr gpu_va;
	int prot;
        int flags;

	int allocation_number;
	char name[32];

	struct list node;
};

void panwrap_track_allocation(mali_ptr gpu_va, int flags, int number, size_t length);
void panwrap_track_mmap(mali_ptr gpu_va, void *addr, size_t length,
                        int prot, int flags);

struct panwrap_mapped_memory *panwrap_find_mapped_mem(void *addr);

#endif /* __MMAP_TRACE_H__ */
